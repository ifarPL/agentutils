package pl.rafsze.utils.agent;

import java.lang.management.ManagementFactory;

import com.sun.tools.attach.VirtualMachine;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class JarAgentLoader extends AgentLoader
{
	private final String agentJar;
	
	private String agentArgs;
	
	public JarAgentLoader agentArgs(String args)
	{
		this.agentArgs = args;
		return this;
	}

	@Override
	@SneakyThrows
	public void load()
	{
		ToolsJarUtils.setup();
		
		VirtualMachine vm = VirtualMachine.attach(getCurrentVirtualMachinePid());
		try
		{
			vm.loadAgent(agentJar, agentArgs);
		}
		finally
		{
			vm.detach();
		}
	}
	
	private static String getCurrentVirtualMachinePid()
	{
		String runtimeName = ManagementFactory.getRuntimeMXBean().getName();
		return runtimeName.substring(0, runtimeName.indexOf('@'));
	}
}
