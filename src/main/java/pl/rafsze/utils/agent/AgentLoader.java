package pl.rafsze.utils.agent;

import com.google.common.base.Preconditions;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AgentLoader
{
	public static JarAgentLoader agentJar(String path)
	{
		Preconditions.checkArgument(path != null, "Path to agent cannot be null");
		return new JarAgentLoader(path);
	}
	
	public static ClassAgentLoader agentClass(Class<?> clazz)
	{
		Preconditions.checkArgument(clazz != null, "Class cannot be null");
		return new ClassAgentLoader(clazz.getName());
	}
	
	public static ClassAgentLoader agentClass(String className)
	{
		Preconditions.checkArgument(className != null, "Class name cannot be null");
		return new ClassAgentLoader(className);
	}
	
	public abstract void load();
}
