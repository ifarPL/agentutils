package pl.rafsze.utils.agent;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ToolsJarUtils
{
	private static boolean isPrepared = false;
	
	public static synchronized void setup()
	{
		if ( !isPrepared )
		{	
			doSetup();
			isPrepared = true;
			log.info("Sucessfully setted up tools.jar to work with AgentLoader");
		}
	}
	
	private static void doSetup()
	{
		try
		{
			Class.forName("com.sun.tools.attach.VirtualMachine", true, ClassLoader.getSystemClassLoader());
			log.debug("Found tools.jar in classpath, no classpath setup is needed");
			return;
		}
		catch ( ClassNotFoundException ignored )
		{
		}
		
		findAndAddToolsToClasspath();
	}
	
	private static void findAndAddToolsToClasspath()
	{
		List<File> possibleJdkHomes = lookForPossibleJdkHomes();
		
		for ( File possibleJdkHome : possibleJdkHomes )
		{
			File tools = lookForToolsInFolder(possibleJdkHome);
			if ( tools != null )
			{
				addToSystemClassPath(tools);
				return;
			}
		}
		
		throw new UnsupportedOperationException("Couldn't find tools.jar, attaching an agent at runtime is not posible in this jvm. " +
				"Make sure you have installed HotSpot based JDK and the JAVA_HOME enviroment variable point to it.");
	}
	
	private static File lookForToolsInFolder(File folder)
	{
		log.debug("Looking in {}/lib for tools.jar", LazyToString.of(() -> folder.getAbsolutePath()));
		if ( !folder.exists() )
		{
			return null;
		}
		
		File tools = new File(folder, "lib/tools.jar");
		if ( tools.exists() )
		{
			return tools;
		}
		
		return null;
	}
	
	@SneakyThrows
	static void addToSystemClassPath(File fileToAdd)
	{
		Method addURL = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
		addURL.setAccessible(true);
		addURL.invoke(ClassLoader.getSystemClassLoader(), fileToAdd.toURI().toURL());
		
		log.debug("Sucessfully added {} to classpath", LazyToString.of(() -> fileToAdd.getAbsolutePath()));
	}
	
	private static List<File> lookForPossibleJdkHomes()
	{
		List<File> possibleJdkHomes = new ArrayList<>(5);
		
		File currentJvmHome = new File(System.getProperty("java.home"));
		possibleJdkHomes.add(currentJvmHome);
		
		if ( currentJvmHome.getName().equals("jre") )
		{
			possibleJdkHomes.add(currentJvmHome.getParentFile());
		}
		
		if ( System.getenv().containsKey("JAVA_HOME") )
		{
			possibleJdkHomes.add(new File(System.getenv("JAVA_HOME")));
		}
		
		if ( System.getenv().containsKey("JDK_HOME") )
		{
			possibleJdkHomes.add(new File(System.getenv("JDK_HOME")));
		}
		
		if ( System.getProperty("agentloader.tools", null) != null )
		{
			possibleJdkHomes.add(new File(System.getProperty("agentloader.tools")));
		}
		
		return possibleJdkHomes;
	}
}



