package pl.rafsze.utils.agent;

import com.google.common.base.Supplier;

import lombok.RequiredArgsConstructor;

/**
 * Simple workaround to Slf4J limitations
 */
@RequiredArgsConstructor(staticName = "of")
class LazyToString
{
	private final Supplier<?> supplier;
	
	@Override
	public String toString()
	{
		return String.valueOf(supplier.get());
	}
}
