package pl.rafsze.utils.agent;

/**
 * Simple utility to run agent without proper java program
 */
class RunAgent
{
	public static void main(String[] args)
	{
		if ( args.length == 0 )
		{
			System.err.println("You need to specify at least one agent jar to run!");
			return;
		}
		
		for ( String agent : args )
		{
			try
			{
				AgentLoader.agentJar(agent).load();
				System.out.println("Sucessfully run agent " + agent);
			}
			catch ( Throwable e )
			{
				System.err.println("Couldn't run agent " + agent);
				e.printStackTrace();
			}
		}
	}
}
