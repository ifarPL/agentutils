package pl.rafsze.utils.agent;

import java.io.File;
import java.io.FileOutputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class ClassAgentLoader extends AgentLoader
{
	private final String className;
	
	private String agentArgs = null;
	
	private boolean canRedefineClasses = true;
	private boolean canRetransformClasses = true;
	private boolean canSetNativeMethodPrefix = false;
	
	public ClassAgentLoader canRedefineClasses(boolean flag)
	{
		this.canRedefineClasses = flag;
		return this;
	}
	
	public ClassAgentLoader canRetransformClasses(boolean flag)
	{
		this.canRetransformClasses = flag;
		return this;
	}
	
	public ClassAgentLoader canSetNativeMethodPrefix(boolean flag)
	{
		this.canSetNativeMethodPrefix = flag;
		return this;
	}
	
	public ClassAgentLoader agentArgs(String agentArgs)
	{
		this.agentArgs = agentArgs;
		return this;
	}
	
	@Override
	@SneakyThrows
	public void load()
	{
		File tempAgentJar = createTempJar();
		AgentLoader.agentJar(tempAgentJar.getAbsolutePath()).agentArgs(agentArgs).load();
	}
	
	@SneakyThrows
	private File createTempJar()
	{
		File tempAgentJar = File.createTempFile("pl.rafsze.utils.agent", ".tmp");
		tempAgentJar.deleteOnExit();
		
		Manifest jarManifest = createManifest();
		
		try ( JarOutputStream jar = new JarOutputStream(new FileOutputStream(tempAgentJar), jarManifest) )
		{
		}
		
		return tempAgentJar;
	}
	
	private Manifest createManifest()
	{
		Manifest result = new Manifest();
		result.getMainAttributes().putValue("Manifest-Version", "1.0");
		result.getMainAttributes().putValue("Agent-Class", className);
		result.getMainAttributes().putValue("Can-Redefine-Classes", canRedefineClasses + "");
		result.getMainAttributes().putValue("Can-Retransform-Classes", canRetransformClasses + "");
		result.getMainAttributes().putValue("Can-Set-Native-Method-Prefix", canSetNativeMethodPrefix + "");
		
		return result;
	}
	
}
