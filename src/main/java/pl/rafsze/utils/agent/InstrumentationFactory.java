package pl.rafsze.utils.agent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.Field;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;

import com.google.common.base.Preconditions;
import com.google.common.io.ByteStreams;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InstrumentationFactory
{
	private static final String AGENT_CLASS_NAME = InstrumentationFactory.class.getName() + "$Agent";
	
	private static Instrumentation instrumentation;

	public static Instrumentation getInstrumentation()
	{
		if ( instrumentation == null )
		{
			instrumentation = obtainInstrumentation();
		}
		
		return instrumentation;
	}
	
	@SneakyThrows
	private static Instrumentation obtainInstrumentation()
	{
		Class<?> agentClass = tryToLoadAgentClassViaSystemClassLoader();
		if ( agentClass == null )
		{
			log.debug("Agent class isn't loaded via system ClassLoader");
			addAgentClassToSystemClassLoader();
			agentClass = tryToLoadAgentClassViaSystemClassLoader();
		}
		
		Preconditions.checkState(agentClass != null);
		AgentLoader.agentClass(AGENT_CLASS_NAME).load();
		
		// as agent class might be not loaded via the same class loader as this class, we cannot refer it directly
		Field instrumentation = agentClass.getDeclaredField("instrumentationInstance");
		instrumentation.setAccessible(true);
		return (Instrumentation) instrumentation.get(null);
	}
	
	private static Class<?> tryToLoadAgentClassViaSystemClassLoader()
	{
		try
		{
			return Class.forName(AGENT_CLASS_NAME, true, ClassLoader.getSystemClassLoader());
		}
		catch ( ClassNotFoundException e )
		{
			return null;
		}
	}
	
	private static void addAgentClassToSystemClassLoader()
	{
		File tempJar = createTempJar();
		ToolsJarUtils.addToSystemClassPath(tempJar);
	}
	
	@SneakyThrows
	private static File createTempJar()
	{
		File jar = File.createTempFile("agentloader.instrumentationfactory", ".tmp");
		jar.deleteOnExit();
		
		try ( JarOutputStream jarOut = new JarOutputStream(new FileOutputStream(jar));
				InputStream in = InstrumentationFactory.class.getClassLoader().getResourceAsStream(AGENT_CLASS_NAME.replace('.', '/') + ".class") )
		{
			jarOut.putNextEntry(new ZipEntry(AGENT_CLASS_NAME.replace('.', '/') + ".class"));
			ByteStreams.copy(in, jarOut);
			jarOut.closeEntry();
		}
		
		return jar;
	}
	
	@SuppressWarnings("unused")
	private static class Agent
	{
		private static Instrumentation instrumentationInstance;

		public static void agentmain(String args, Instrumentation inst)
		{
			instrumentationInstance = inst;
		}
	}
}
