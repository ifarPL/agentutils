package pl.rafsze.utils.agent.test;

import java.lang.instrument.Instrumentation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

import pl.rafsze.utils.agent.AgentLoader;

@Slf4j
public class ClassAgentLoaderTest
{
	private static boolean agentHasBeenExecuted;
	
	@Before
	public void prepare()
	{
		agentHasBeenExecuted = false;
	}
	
	@Test
	public void testClassAgentLoaderWithClassObject()
	{
		AgentLoader.agentClass(TestAgent.class).agentArgs("Some args").load();
		
		Assert.assertTrue(agentHasBeenExecuted);
	}
	
	@Test
	public void testClassAgentLoaderWithClassName()
	{
		AgentLoader.agentClass("pl.rafsze.utils.agent.test.ClassAgentLoaderTest$TestAgent").agentArgs("Some args").load();
		
		Assert.assertTrue(agentHasBeenExecuted);
	}
	
	private static class TestAgent
	{	
		@SuppressWarnings("unused")
		public static void agentmain(String agentArgs, Instrumentation inst)
		{
			agentHasBeenExecuted = true;
			log.info("Inside angent");
		}
	}
}

