package pl.rafsze.utils.agent.test;

import java.lang.instrument.Instrumentation;

import org.junit.Assert;
import org.junit.Test;

import pl.rafsze.utils.agent.InstrumentationFactory;

public class InstrumentationFactoryTest
{
	@Test
	public void testInstrumentationFactory()
	{
		Instrumentation instrumentation = InstrumentationFactory.getInstrumentation();
		
		Assert.assertNotNull(instrumentation);
		
		Instrumentation instrumentation2 = InstrumentationFactory.getInstrumentation();
		Assert.assertTrue(instrumentation == instrumentation2);
	}
}
